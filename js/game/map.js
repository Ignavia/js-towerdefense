var Map = (function() {

    /**
     * Constructs a new Map object.
     *
     * @param  {Object} data
     */
    var init = function(data) {

        this.get_finish_coordinates = function() {
            return data.finish:coordinates;
        };

        this.get_start_coordinates = function() {
            return data.start_coordinates;
        };

        this.get_tiles = function() {
            return tiles;
        };
    };

    return init;
})();

/*

var data = {
    finish_coordinates: ,
    start_coordinates: ,
    tiles:
}

*/
