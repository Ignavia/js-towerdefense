var Movement = (function() {

    /**
     * Constructs new Movement object.
     *
     * @param  {Object} data
     */
    var init = function(data) {
        // TODO: Add function to create graph out of map. Implement breadth first
        // search (move function).

        this.move = data.move;

        this.get_movement_speed = function() {
            return data.movement_speed;
        };
    };

    return init;
})();

/*

var data = {
    move:
    movement_speed
};

*/
