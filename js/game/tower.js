// TODO: Put everything related to towers here.
var Tower = (function() {

    var init = function(data) {

        /**
         * The current hitpoints of the object.
         *
         * @type {Integer}
         */
        var current_hitpoints = data.hitpoints;

        var effects = [];

        var tick_effects = function() {
            for (var i = 0; i < effects.length; i++) {
                effects[i].tick(this);
            }
        };

        var use_skills = function(creeps, towers) {
            for (var i = 0; i < data.skills.length; i++) {
                data.skills[i].use(this, creeps, towers);
            }
        };

        this.add_effect = function(effect) {
            effects.push(effect);
        };

        this.execute = function(creeps, towers) {
            tick_effects();
            use_skills(creeps, towers); // TODO: Think about order
        };

        this.get_coordinates = function() {
            return data.coordinates;
        };

        this.get_current_hitpoints = function() {
            return current_hitpoints;
        };

        this.get_description = function() {
            return data.description;
        }

        this.get_hitpoints = function() {
            return data.hitpoints;
        };

        this.get_icon = function() {
            return data.icon;
        };

        this.get_skills = function() {
            return data.skills;
        };

        this.remove_effect = function(effect) {
            var index = effect.indexOf(effect);
            if (index > -1) {
                effects.splice(index, 1);
            }
        };
    };

    return init;
})();

/*

Structure of data object:

var data = {
    coordinates: ,
    description: ,
    hitpoints: ,
    icon: ,
    skills:
};

How to create a new Tower object:

var tower = new Tower(data);

*/
