var Graph = (function() {
    var init = function(vertices) {
        var bfs = function(start) {
            var i;

            // Init
            for (i = 0; i < vertices.length; i++) {
                vertices[i].is_new = true;
                vertices[i].level = Infinity;
                vertices[i].predecessor = null;
            }
            start.is_new = false;
            start.level = 0;
            var queue = [start];

            // Visit
            while (queue.length > 0) {
                var current = queue.shift();
                for (i = 0; i < current.adjacent_vertices.length; i++) {
                    var destination = current.adjacent_vertices[i];
                    if (destination.is_new) {
                        destination.is_new = false;
                        destination.level = current.level + 1;
                        destination.predecessor = current;
                        queue.push(destination);
                    }
                }
            }
        };

        this.compute_next_step = function(start, finishes) {
            bfs(start);

            // Find nearest finish
            var nearest_finish = finishes[0];
            for (var i = 1; i < finishes.length; i++) {
                if (finishes[i].level < nearest_finish.level) {
                    nearest_finish = finishes[i];
                }
            }

            // Find path to nearest finish
            var current = nearest_finish;
            if (current === start) {
                return null;
            }
            while (current.predecessor != start) {
                current = current.predecessor;
            }
            return current;
        };

        this.is_blocked = function(starts, finishes) {
            for (var i = 0; i < starts.length; i++) {
                bfs(starts[i]);
                var is_blocked = true;
                for (var j = 0; j < finishes.length; j++) {
                    if (finishes[j].level < Infinity) {
                        is_blocked = false;
                        break;
                    }
                }
                if (is_blocked) {
                    return true;
                }
            }
            return false;
        };
    };

    init.generate_graph = function(map, movement) {

    };

    return init;
})();

var Vertex = (function() {
    var id_counter = 0;

    var init = function() {
        this.adjacent_vertices = [];
        this.id = id_counter++;
    };

    return init;
})();
