var Effect = (function() {

    /**
     * Constructs a new Effect object.
     */
    var init = function(data) {

        this.apply = function(unit) {
            if (data.duration > 0) {
                data.apply(unit);
                unit.add_effect(this);
            }
        };

        this.tick = function(unit) {
            data.tick(unit);
            data.duration--;
            if (data.duration === 0) {
                remove(unit);
            }
        };

        this.remove = function(unit) {
            data.remove(unit);
            unit.remove_effect(this);
        };
    };

    return init;
})();

/*

var data = {
    apply: ,
    tick: ,
    remove: ,
}

*/
