// TODO: Put everything related to creeps here.
var Creep = (function() {

    /**
     * Constructs a new Creep object.
     */
    var init = function(data) {
        var current_hitpoints = data.hitpoints;
        var effects = [];

        var move = function(map) {
            data.movement.move(this, map);
        };

        var tick_effects = function() {
            for (var i = 0; i < effects.length; i++) {
                effects[i].tick(this);
            }
        };

        this.add_effect = function(effect) {
            effects.push(effect);
        };

        this.execute = function(map, creeps, towers) {
            tick_effects();
            move(map);
        };

        this.get_coordinates = function() {
            return data.coordinates;
        };

        this.get_current_hitpoints = function() {
            return current_hitpoints;
        };

        this.get_hitpoints = function() {
            return data.hitpoints;
        };

        this.remove_effect = function(effect) {
            var index = effect.indexOf(effect);
            if (index > -1) {
                effects.splice(index, 1);
            }
        };

        this.set_coordinates = function(coordinates) {
            data.coordinates = coordinates;
        };
    };

    return init;
})();

/*

var data = {
    coordinates: ,
    hitpoints: ,
    movement: ,
}

*/
