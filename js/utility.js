/**
 * A point in a two dimensional plane. The origin is in the top left corner.
 * The x-axis is pointing to the right, the y-axis is pointing down.
 *
 * @namespace UTILITY
 * @class Point
 * @constructor
 * @param {Number} x The x-coordinate of the point.
 * @param {Number} y The y-coordinate of the point.
 * @author Lars Reimann
 * @version 0.1
 *
 * @example
 * <pre><code>var origin = new Point(0, 0);</code></pre>
 */
TOWERDEFENSE.UTILITY.Point = (function () {
    "use strict";

    var init = function (x, y) {

        /**
         * The x-coordinate of the point.
         *
         * @property x
         * @type {Number}
         */
        this.x = x;

        /**
         * The y-coordinate of the point.
         *
         * @property y
         * @type {Number}
         */
        this.y = y;
    };

    /**
     * Creates a copy of the provided Point object. The new point has the same
     * coordinates as the old point, but they are not the same objects. More
     * formally the following relations hold:
     * <ul>
     *     <li>new_point.x === old_point.x</li>
     *     <li>new_point.y === old_point.y</li>
     *     <li>new_point != old_point</li>
     * </ul>
     *
     * @static
     * @method copy
     * @param {Point} point The point to be copied.
     * @return {Point} A copy of the provided point.
     *
     * @example
     * <pre><code>var point = new Point(1, 2);
     * var copy = Point.copy(point);</code></pre>
     */
    init.copy = function (point) {
        return new TOWERDEFENSE.UTILITY.Point(point.x, point.y);
    };

    /**
     * Computes the euclidean distance between the given points.
     *
     * @static
     * @method distance
     * @param {Point} p1 The first point.
     * @param {Point} p2 The second point.
     * @return {Number} The euclidean distance between the two points.
     *
     * @example
     * <pre><code>var p1 = new Point(0, 0);
     * var p2 = new Point(0, 1);
     * var distance = Point.distance(p1, p2); // expected to be equal to 1</code></pre>
     */
    init.distance = function (p1, p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    };

    return init;
}());

/**
 * A new Dimension object bundles a width and a height together and can be used
 * to describe a rectangular shape.
 *
 * @namespace UTILITY
 * @class Dimension
 * @constructor
 * @param {Number} width The width dimension.
 * @param {Number} height The height dimension.
 * @author Lars Reimann
 * @version 0.1
 *
 * @example
 * <pre><code>var square = Dimension(1, 1);</code></pre>
 */
TOWERDEFENSE.UTILITY.Dimension = (function () {
    "use strict";

    var init = function (width, height) {

        /**
         * The width dimension.
         *
         * @property width
         * @type {Number}
         */
        this.width = width;

        /**
         * The height dimension.
         *
         * @property height
         * @type {Number}
         */
        this.height = height;
    };

    /**
     * Creates a copy of the provided Dimension object. The new dimension has the
     * same width and height as the old dimension, but they are not the same
     * objects. More formally the following relations hold:
     * <ul>
     *     <li>new_dimension.width === old_dimension.width</li>
     *     <li>new_dimension.height === old_dimension.height</li>
     *     <li>new_dimension != old_dimension</li>
     * </ul>
     *
     * @static
     * @method copy
     * @param {Dimension} point The dimension to be copied.
     * @return {Dimension} A copy of the provided dimension.
     *
     * @example
     * <pre><code>var dimension = new Dimension(1, 2);
     * var copy = Dimension.copy(dimension);</code></pre>
     */
    init.copy = function (dimension) {
        return new TOWERDEFENSE.UTILITY.Dimension(dimension.width, dimension.height);
    };

    return init;
}());
